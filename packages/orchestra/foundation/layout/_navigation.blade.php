<?

use Illuminate\Support\Facades\Auth;
use Orchestra\Support\Facades\App; ?>

<header class="navbar clearfix navbar-fixed-top" id="header" role="navigation">
	<div class="container">
		<div class="navbar-brand">
		  <a href="{{ handles('orchestra::/') }}">
		    <img src="{{ Orchestra\Theme::asset('img/logo/logo.png') }}" alt="{{ memorize('site.name', 'Orchestra Platform') }}" class="img-responsive" height="30" width="120" />
      </a>
      <div id="sidebar-collapse" class="sidebar-collapse btn">
        <i class="fa fa-bars" data-icon1="fa fa-bars" data-icon2="fa fa-bars" ></i>
      </div>
		</div>
			@include('orchestra/foundation::components.usernav')
	</div>
</header>

@unless (Auth::check())
<script>
jQuery(function ($) {
	$('a[rel="user-menu"]').on('click', function (e) {
		e.preventDefault();

		window.location.href = "{{ handles('orchestra::login') }}";

		return false;
	});
});
</script>
@endunless
