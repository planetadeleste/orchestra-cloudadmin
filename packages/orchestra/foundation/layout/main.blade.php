<?
  $class = '';
  if (Request::is('admin/*')){
    $aclass = explode('/', Request::path());
    array_shift($aclass);
    $class = join('-', $aclass);
  }
  Orchestra\Support\Facades\Site::set('header::class', 'page-header')
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		@include('orchestra/foundation::layout._header')
	</head>
	<body class="{{ $class }}">
	  @if(Auth::check())
		  @include('orchestra/foundation::layout._navigation')
		@endif
		<section id="page">
    @if(Auth::check())
      @include('orchestra/foundation::layout._sidebar')
      <div id="main-content">
        <div class="container">
          <div class="row">
            <div id="content" class="col-lg-12">
              <div class="row">
                <div class="col-sm-12">
                  @include('orchestra/foundation::components.header')
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  @include('orchestra/foundation::components.messages')
                  @yield('content')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @else
      @include('orchestra/foundation::components.messages')
      @yield('content')
    @endif
		</section>
		@include('orchestra/foundation::layout._footer')

		@if(Auth::check())
		<script>
    		jQuery(document).ready(function() {
    			App.setPage("fixed_header_sidebar");  //Set current page
    			App.init(); //Initialise plugins and elements
    		});
    	</script>
		@endif
	</body>
</html>
