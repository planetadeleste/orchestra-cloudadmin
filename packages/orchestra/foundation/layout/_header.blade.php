<?

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\HTML;
use Orchestra\Support\Facades\Asset;
use Orchestra\Support\Facades\Theme;
?>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
{{ HTML::title() }}
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<meta name="description" content="Teros CMS">
<meta name="author" content="teros.uy">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
  <script src="{{ Orchestra\Theme::asset('js/flot/excanvas.min.js') }}"></script>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<?

$asset = Asset::container('orchestra/foundation::header');

$asset->style('select2', 'packages/orchestra/foundation/vendor/select2/select2.css');
$asset->style('jquery-ui', 'packages/orchestra/foundation/vendor/delta/theme/jquery-ui.css');
$asset->style('bootstrap', 'packages/orchestra/foundation/vendor/bootstrap/css/bootstrap.min.css');
$asset->style('font-awesome', Theme::asset('css/font-awesome/css/font-awesome.css'), array('bootstrap'));
$asset->style('daterangepicker', Theme::asset('js/bootstrap-daterangepicker/daterangepicker-bs3.css'), array('bootstrap'));
$asset->style('uniform', Theme::asset('js/uniform/css/uniform.default.min.css'));
$asset->style('animate', Theme::asset('css/animatecss/animate.min.css'));
//$asset->style('orchestra', 'packages/orchestra/foundation/css/orchestra.css', array('bootstrap', 'select2'));
$asset->style('cloudadmin', Theme::asset('css/cloudadmin.css'));


$asset->script('underscore', 'packages/orchestra/foundation/vendor/underscore.min.js');
$asset->script('jquery', Theme::asset('js/jquery/jquery.min.js'));
$asset->script('javie', 'packages/orchestra/foundation/vendor/javie/javie.min.js', array('jquery', 'underscore'));



if(Auth::check()){
  $asset->style('orchestra', Theme::asset('css/orchestra.css'), array('cloudadmin'));
  $asset->style('theme', Theme::asset('css/graphite.css'), array('cloudadmin'));
  $asset->style('responsive', Theme::asset('css/responsive.css'), array('cloudadmin'));
}

?>

{{ $asset->styles() }}

{{-- FONTS --}}
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>

{{ $asset->scripts() }}

<script>
Javie.ENV = "{{ App::environment() }}";
</script>

@placeholder("orchestra.layout: header")

