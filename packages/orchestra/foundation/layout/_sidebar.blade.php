<? use Orchestra\Support\Facades\App; ?>
<div id="sidebar" class="sidebar sidebar-fixed">
  <div class="sidebar-menu nav-collapse">
    <div class="divide-20"></div>
    {{-- SEARCH BAR --}}
    <div id="search-bar">
      <input class="search" type="text" placeholder="Search"><i class="fa fa-search search-icon"></i>
    </div>
    {{-- /SEARCH BAR --}}

    {{-- SIDEBAR MENU --}}
    @include('orchestra/foundation::components.menu', array('menu' => App::menu('orchestra')))
    {{-- /SIDEBAR MENU --}}
  </div>
</div>