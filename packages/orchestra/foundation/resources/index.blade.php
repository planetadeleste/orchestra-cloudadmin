@extends('orchestra/foundation::layout.main')

@section('content')
<div class="row">
	<div class="col-md-12">
	  <div class="box border">
      <div class="box-title"></div>
        <div class="box-body big">
        {{ $table }}
        </div>
      </div>
	</div>
</div>
@stop
