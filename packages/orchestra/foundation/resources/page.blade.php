@extends('orchestra/foundation::layout.main')

@section('content')
<div class="row">
	<div class="col-md-3">
		@include('orchestra/foundation::resources._list')
		@placeholder("orchestra.resources: {$resources['name']}")
		@placeholder('orchestra.resources')
	</div>
	<div class="col-md-8">
		{{ $content }}
	</div>
</div>
@stop
