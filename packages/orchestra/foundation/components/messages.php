<?php 

$message  = Orchestra\Support\Facades\Messages::retrieve();
$messages = '';

if ($message instanceof Orchestra\Support\Messages) :
	foreach (array('error', 'info', 'success') as $key) :
		if ($message->has($key)) :
			$message->setFormat(
				'<div class="alert alert-'.$key.'">:message<button class="close" data-dismiss="alert">×</button></div>'
			);

			$messages .= implode('', $message->get($key));
		endif;
	endforeach;
endif;

if(!empty($messages)): ?>
  <header>
    <div class="container"><?php echo $messages; ?></div>
  </header>
<?php endif;
