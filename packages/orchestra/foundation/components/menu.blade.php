<ul>
	@foreach ($menu as $item)
		@if (1 > count($item->childs))
			<li>
				<a href="{{ $item->link }}">
					<span class="menu-text">{{ $item->title }}</span>
				</a>
			</li>
		@else
			<li class="has-sub">
				<a href="javascript:;">
				  <span class="menu-text">{{ $item->title }}</span>
				  <span class="arrow"></span>
        </a>
				<ul class="sub">
					<li>
						<a href="{{ $item->link }}">
							<span class="sub-menu-text">{{ $item->title }}</span>
						</a>
					</li>
					<li class="divider"></li>
					@foreach ($item->childs as $child)
						<? $grands = $child->childs; ?>
						<li class="{{ ( ! empty($grands) ? "has-sub-sub" : "" ) }}">
							<a href="{{ $child->link }}">
								<span class="sub-menu-text">{{ $child->title }}</span>
							</a>
							@if ( ! empty($child->childs))
							<ul class="sub-sub">
								@foreach ($child->childs as $grand)
								<li>
									<a href="{{ $grand->link }}">
										<span class="sub-sub-menu-text">{{ $grand->title }}</span>
									</a>
								</li>
								@endforeach
							</ul>
							@endif
						</li>
					@endforeach
				</ul>
			</li>
		@endif
	@endforeach
</ul>
