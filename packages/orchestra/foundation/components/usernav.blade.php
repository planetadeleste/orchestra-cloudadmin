<?

use Illuminate\Support\Facades\Auth;
use Orchestra\Support\Facades\Site;

$user = Auth::user(); ?>

@if (Site::get('navigation::usernav', true))
<ul class="nav navbar-nav navbar-right">
  <li class="dropdown user" id="header-user">
    <a href="#header-user" rel="user-menu" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-wrench fa-x2"></i>
      <span class="username">{{ $user->fullname or trans('orchestra/foundation::title.login') }}</span>
      <i class="fa fa-angle-down"></i>
    </a>
    @unless (is_null($user))
    <ul class="dropdown-menu">
      <li>
        <a href="{{ handles('orchestra::account') }}">
          <i class="fa fa-user"></i> {{ trans('orchestra/foundation::title.account.profile') }}
        </a>
      </li>
      <li>
        <a href="{{ handles('orchestra::account/password') }}">
          <i class="fa fa-cog"></i> {{ trans('orchestra/foundation::title.account.password') }}
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="{{ handles('orchestra::logout') }}">
          <i class="fa fa-power-off"></i> {{ trans('orchestra/foundation::title.logout') }}
        </a>
      </li>
    </ul>
    @endunless
  </li>
</ul>
@endif
