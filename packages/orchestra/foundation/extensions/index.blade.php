@extends('orchestra/foundation::layout.main')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@include('orchestra/foundation::extensions._table')
	</div>
</div>
@stop
