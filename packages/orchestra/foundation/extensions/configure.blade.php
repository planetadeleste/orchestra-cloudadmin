@extends('orchestra/foundation::layout.main')

@section('content')
<div class="row">
	<div class="col-md-8">
	  <div class="box border">
	    <div class="box-title"></div>
	    <div class="box-body big">
		  {{ $form }}
		  </div>
		</div>
	</div>
	<div class="col-md-4">
	  <div class="box border">
      <div class="box-title"></div>
      <div class="box-body">
      @placeholder('orchestra.extensions')
      @placeholder('orchestra.helps')
      </div>
    </div>
	</div>
</div>
@stop
