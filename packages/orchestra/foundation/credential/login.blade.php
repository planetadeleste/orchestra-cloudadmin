@extends('orchestra/foundation::layout.extra')

<?
use Illuminate\Support\Facades\Form;
use Illuminate\Support\Facades\Input;
use Orchestra\Support\Facades\Site; ?>

@section('content')
<header>
  {{-- NAV-BAR --}}
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div id="logo">
          <a href="{{ handles('orchestra::/') }}"><img src="{{ Orchestra\Theme::asset('img/logo/logo-alt.png') }}" height="40" alt="{{ memorize('site.name', 'Orchestra Platform') }}" /></a>
        </div>
      </div>
    </div>
  </div>
  {{-- /NAV-BAR --}}
</header>

{{-- LOGIN --}}
<section id="login" class="visible">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-box-plain">
          <h2 class="bigintro">{{ trans('orchestra/foundation::title.login') }}</h2>
          <div class="divide-40"></div>

          {{ Form::open(array('url' => handles('orchestra::login'), 'action' => 'POST', 'role' => 'form')) }}

            <div class="form-group{{ $errors->has('email') ? ' error' : '' }}">
              {{ Form::label('email', trans("orchestra/foundation::label.users.email"), array('class' => 'control-label')) }}
              <i class="fa fa-envelope"></i>
              {{ Form::input('text', 'email', Input::old('email'), array('required' => true, 'tabindex' => 1, 'class' => 'form-control')) }}
              {{ $errors->first('email', '<p class="help-block">:message</p>') }}
            </div>

            <div class="form-group{{ $errors->has('password') ? ' error' : '' }}">
              {{ Form::label('password', trans('orchestra/foundation::label.users.password'), array('class' => 'control-label')) }}
              <i class="fa fa-lock"></i>
              {{ Form::input('password', 'password', '', array('required' => true, 'tabindex' => 2, 'class' => 'form-control')) }}
              {{ $errors->first('password', '<p class="help-block">:message</p>') }}
            </div>

            <div class="form-actions">
              <label class="checkbox">
                {{ Form::checkbox('remember', 'yes', false, array('tabindex' => 3)) }}
                {{ trans('orchestra/foundation::title.remember-me') }}
              </label>
              <button type="submit" class="btn btn-danger">{{ trans('orchestra/foundation::title.login') }}</button>
            </div>

          {{ Form::close() }}

          <div class="login-helpers">
            <a href="#" onclick="swapScreen('forgot');return false;">{{ trans('orchestra/foundation::title.forgot-password') }}</a>
            @if (memorize('site.registrable', false))
            <br />
            <a href="{{ handles('orchestra::register') }}">
              {{ trans('orchestra/foundation::title.register') }}
            </a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{{-- /LOGIN --}}

{{-- FORGOT PASSWORD --}}
<section id="forgot">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-box-plain">
          <h2 class="bigintro">{{ trans('orchestra/foundation::title.reset-password') }}</h2>
          <div class="divide-40"></div>
          {{ Form::open(array('url' => handles('orchestra::forgot'), 'method' => 'POST', 'class' => 'form-horizontal')) }}
            <div class="form-group{{ $errors->has('email') ? ' error' : '' }}">
              {{ Form::label('email', trans('orchestra/foundation::label.users.email'), array('class' => 'control-label')) }}
              <i class="fa fa-envelope"></i>
              {{ Form::input('email', 'email', Input::old('email'), array('required' => true, 'class' => 'form-control')) }}
              {{ $errors->first('email', '<p class="help-block">:message</p>') }}
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-info">{{ Site::get('title', 'Submit') }}</button>
            </div>
          {{ Form::close() }}
          <div class="login-helpers">
            <a href="#" onclick="swapScreen('login');return false;">{{ trans('orchestra/foundation::title.back-to-login') }}</a> <br>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{{-- /FORGOT PASSWORD --}}

<script>
  jQuery(document).ready(function() {
    App.setPage("login");  //Set current page
    App.init(); //Initialise plugins and elements
  });
</script>
<script type="text/javascript">
  function swapScreen(id) {
    jQuery('.visible').removeClass('visible animated fadeInUp');
    jQuery('#'+id).addClass('visible animated fadeInUp');
  }
</script>

@stop
